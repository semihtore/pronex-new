#!/usr/bin/env sh

. /etc/rc.subr

name="prhotspot"
rcvar="prhotspot_enable"
start_cmd="prhotspot_start"
stop_cmd="prhotspot_stop"

pidfile="/var/run/nginx-PRHotspot.pid"
config_file="/usr/local/prhotspot/install/nginx-PRHotspot.conf"

load_rc_config ${name}

prhotspot_start()
{
if checkyesno ${rcvar}; then
    if [ -f $pidfile ]; then
        echo "PRHotspot already running."
        prhotspot_stop
    fi
    echo -n "PRhotspot starting..."
    /usr/local/sbin/nginx -c ${config_file}
    echo " Done."
fi
}

prhotspot_stop()
{
if [ -f $pidfile ]; then
        echo -n "PRHotspot stopping."
        pkill -F $pidfile
        while [ `pgrep -F $pidfile` ]; do
            echo -n "."
            sleep 5
        done
        rm $pidfile
        echo " Done."
    fi
}

run_rc_command "$1"