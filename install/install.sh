#!/usr/bin/env sh

main() {

if [ ${USER} != "root" ]; then
        echo "Please login \"root\" user. Not \"admin\" user !"
        exit
fi

if [ -f /etc/platform ]; then
	if [ `cat /etc/platform` = "pfSense" ]; then
		OS_NAME=pfSense
		OS_VERSION=`cat /etc/version`
		OS_VERSION_MAJOR=`cat /etc/version | awk -F. '{print $1}'`
		OS_VERSION_MINOR=`cat /etc/version | awk -F. '{print $2}'`
		OS_VERSION_REVISION=`cat /etc/version | awk -F. '{print $3}'`

		if [ ${OS_VERSION_MAJOR} != "2" ] || [ ${OS_VERSION_MINOR} -lt "3" ]; then
            echo "Are you sure this operating system is pfSense 2.3.x or later? This installation only works in version 2.3.x or later"
            exit
		fi
		else
		    echo "Are you sure this operating system is pfSense?"
	fi
	else
        echo "Are you sure this operating system is pfSense?"
        exit
fi

START_PATH=${PWD}
touch ${START_PATH}/prhotspot.log
OUTPUTLOG=${START_PATH}/prhotspot.log

# Defaults
PR_LANG_DEFAULT="en"
PR_PORT_DEFAULT="3003"
PR_MYSQL_ROOT_PASS_DEFAULT="prhotspot"
PR_MYSQL_USER_NAME_DEFAULT="prhotspot"
PR_MYSQL_USER_PASS_DEFAULT="prhotspot"
PR_MYSQL_DBNAME_DEFAULT="prhotspot"
PR_ZONE_NAME_DEFAULT="PRHOTSPOT"

_selectLanguage

printf "\033c"

# Gerekli paketler kuruluyor...
_installPackages

echo -e ${L_WELCOME}
echo

# User Inputs
_userInputs

echo
echo ${L_STARTING}
echo

exec 3>&1 1>>${OUTPUTLOG} 2>&1

# QHotspot Repodan cekiliyor...
_clonePrHotspot

# MySQL 5.6 Server paketi kuruluyor...
_mysqlInstall

_mysqlSettings

# QHotspot nginx 81 port eklemesi yapiliyor...
_nginxSettings

# freeRADIUS3 kuruluyor...
_radiusInstall

# Cron kuruluyor...
_cronInstall

# QHotspot Konfigurasyon yukleniyor...
_prhotspotSettings

# 5651 Sayılı kanun gereği loglama

_5651
# Temizlik


if $( YesOrNo "${L_QUNIFIINSTALL}"); then 1>&3
    echo -n ${L_UNIFICONTROLLER} 1>&3
    fetch -o - https://git.io/j7Jy | sh -s
    echo ${L_OK} 1>&3
fi
if $( YesOrNo "${L_QRESTARTPFSENSE}"); then 1>&3
        echo ${L_RESTARTPFSENSE} 1>&3
        /sbin/reboot
else
        cd /usr/local/prhotspot
fi
}

_selectLanguage() {
    read -p "Select your language (en/tr) [$PR_LANG_DEFAULT]: " PR_LANG
    PR_LANG="${PR_LANG:-$PR_LANG_DEFAULT}"
    case "${PR_LANG}" in
            [eE][nN])
            fetch https://bitbucket.org/semihtore/pronex-new/raw/master/install/lang_en.inc
            . lang_en.inc
            ;;
            [tT][rR])
            fetch https://bitbucket.org/semihtore/pronex-new/raw/master/install/lang_tr.inc
            . lang_tr.inc
            ;;
    esac
}

 _userInputs() {
    read -p "$L_QPORT [$PR_PORT_DEFAULT]: " PR_PORT
    PR_PORT="${PR_PORT:-$PR_PORT_DEFAULT}"
    read -p "$L_QROOTPASS [$PR_MYSQL_ROOT_PASS_DEFAULT]: " PR_MYSQL_ROOT_PASS
    PR_MYSQL_ROOT_PASS="${PR_MYSQL_ROOT_PASS:-$PR_MYSQL_ROOT_PASS_DEFAULT}"
    read -p "$L_QRADIUSUSERNAME [$PR_MYSQL_USER_NAME_DEFAULT]: " PR_MYSQL_USER_NAME
    PR_MYSQL_USER_NAME="${PR_MYSQL_USER_NAME:-$PR_MYSQL_USER_NAME_DEFAULT}"
    read -p "$L_QRADIUSPASSWORD [$PR_MYSQL_USER_PASS_DEFAULT]: " PR_MYSQL_USER_PASS
    PR_MYSQL_USER_PASS="${PR_MYSQL_USER_PASS:-$PR_MYSQL_USER_PASS_DEFAULT}"
    read -p "$L_QRADIUSDBNAME [$PR_MYSQL_DBNAME_DEFAULT]: " PR_MYSQL_DBNAME
    PR_MYSQL_DBNAME="${PR_MYSQL_DBNAME:-$PR_MYSQL_DBNAME_DEFAULT}"
    read -p "$L_QZONENAME [$PR_ZONE_NAME_DEFAULT]: " PR_ZONE_NAME
    PR_ZONE_NAME="${PR_ZONE_NAME:-$PR_ZONE_NAME_DEFAULT}"
}

_5651()
     fetch -o 5651.sh https://bitbucket.org/rembilgisayar/pronex/raw/master/install/5651.sh && sh 5651.sh

_activeRepos() {
    echo -n ${L_ACTIVEREPOS} 1>&3
    tar xv -C / -f /usr/local/share/pfSense/base.txz ./usr/bin/install
    if [ ${OS_VERSION_MINOR} -lt "4" ]; then
        sed -i .bak -e "s/FreeBSD: { enabled: no/FreeBSD: { enabled: yes/g" /usr/local/etc/pkg/repos/pfSense.conf
    else
        sed -i .bak -e "s/FreeBSD: { enabled: no/FreeBSD: { enabled: yes/g" /usr/local/share/pfSense/pkg/repos/pfSense-repo.conf
    fi
    sed -i .bak -e "s/FreeBSD: { enabled: no/FreeBSD: { enabled: yes/g" /usr/local/etc/pkg/repos/FreeBSD.conf
    env ASSUME_ALWAYS_YES=YES /usr/sbin/pkg update
    echo ${L_OK} 1>&3
}

_deactiveRepos() {
    echo -n ${L_DEACTIVEREPOS} 1>&3
    if [ ${OS_VERSION_MINOR} -lt "4" ]; then
        sed -i .bak -e "s/FreeBSD: { enabled: yes/FreeBSD: { enabled: no/g" /usr/local/etc/pkg/repos/pfSense.conf
    else
        sed -i .bak -e "s/FreeBSD: { enabled: yes/FreeBSD: { enabled: no/g" /usr/local/share/pfSense/pkg/repos/pfSense-repo.conf
    fi
    sed -i .bak -e 's/FreeBSD: { enabled: yes/FreeBSD: { enabled: no/g' /usr/local/etc/pkg/repos/FreeBSD.conf
    echo ${L_OK} 1>&3
}

_installPackages() {

if [ ! -f ${PWD}/restarted.qhs ]; then
    exec 3>&1 1>>${OUTPUTLOG} 2>&1
    # FreeBSD ve pfSense paketleri aktif ediliyor...
    _activeRepos

    echo -n ${L_INSTALLPACKAGES} 1>&3
    ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')
    if [ ${ARCH} == "amd64" ]
    then
    env ASSUME_ALWAYS_YES=YES /usr/sbin/pkg install git wget nano mc htop mysql56-server compat8x-amd64 php72-mysqli php72-pdo_mysql php72-soap
    else
    env ASSUME_ALWAYS_YES=YES /usr/sbin/pkg install git wget nano mc htop mysql56-server compat8x-i386 php72-mysqli php72-pdo_mysql php72-soap
    fi
    hash -r
    echo ${L_OK} 1>&3

    touch ${PWD}/restarted.qhs
    echo -e ${L_RESTARTMESSAGE} 1>&3
    echo ${L_PRESSANYKEY} 1>&3
    read -p "restart" answer
      /sbin/reboot

fi
}

_clonePrHotspot() {
    echo -n ${L_CLONEQHOTSPOT} 1>&3
    cd /usr/local
    git clone https://semihtore@bitbucket.org/semihtore/pronex-new.git prhotspot
    cd /usr/local/prhotspot
    cd /usr/local/prhotspot/install
    echo ${L_OK} 1>&3
}

_mysqlInstall() {
    echo -n ${L_MYSQLINSTALL} 1>&3
    if [ ! -f /etc/rc.conf.local ] || [ $(grep -c mysql_enable /etc/rc.conf.local) -eq 0 ]; then
        echo 'mysql_enable="YES"' >> /etc/rc.conf.local
    fi
    mv /usr/local/etc/rc.d/mysql-server /usr/local/etc/rc.d/mysql-server.sh
    sed -i .bak -e 's/mysql_enable="NO"/mysql_enable="YES"/g' /usr/local/etc/rc.d/mysql-server.sh
    service mysql-server.sh start
    echo ${L_OK} 1>&3
}

_mysqlSettings() {
    # MySQL root kullanicisi tanimlaniyor.
    echo -n ${L_MYSQLROOT} 1>&3
    mysqladmin -u root password "${PR_MYSQL_ROOT_PASS}"
    echo ${L_OK} 1>&3

    # MySQL veritabani yukleniyor
    echo -n ${L_MYSQLINSERTS} 1>&3
    cat <<EOF > /usr/local/prhotspot/install/client.cnf
[client]
user = root
password = ${PR_MYSQL_ROOT_PASS}
host = localhost
EOF
    sed -i .bak -e "s/{PR_MYSQL_USER_NAME}/$PR_MYSQL_USER_NAME/g" /usr/local/prhotspot/web/inc/db_settings.php
    sed -i .bak -e "s/{PR_MYSQL_USER_PASS}/$PR_MYSQL_USER_PASS/g" /usr/local/prhotspot/web/inc/db_settings.php
    sed -i .bak -e "s/{PR_MYSQL_DBNAME}/$PR_MYSQL_DBNAME/g" /usr/local/prhotspot/web/inc/db_settings.php

    sed -i .bak -e "s/{PR_MYSQL_ROOT_PASS}/$PR_MYSQL_ROOT_PASS/g" /usr/local/prhotspot/install/prhotspot.sql
    sed -i .bak -e "s/{PR_MYSQL_USER_NAME}/$PR_MYSQL_USER_NAME/g" /usr/local/prhotspot/install/prhotspot.sql
    sed -i .bak -e "s/{PR_MYSQL_USER_PASS}/$PR_MYSQL_USER_PASS/g" /usr/local/prhotspot/install/prhotspot.sql
    sed -i .bak -e "s/{PR_MYSQL_DBNAME}/$PR_MYSQL_DBNAME/g" /usr/local/prhotspot/install/prhotspot.sql

    mysql --defaults-extra-file=/usr/local/prhotspot/install/client.cnf < /usr/local/prhotspot/install/prhotspot.sql
    echo ${L_OK} 1>&3

    # MySQL icin watchdog scripti olusturuluyor.
    echo -n ${L_MYSQLWATCHDOG} 1>&3
    cat <<EOF > /usr/local/bin/prhotspot_check.sh
#!/usr/bin/env sh
service mysql-server.sh status
if [ \$? != 0 ]; then
service mysql-server.sh start
sleep 5
fi
if ! [ -f /var/run/radiusd.pid ]; then
service radiusd onestart
fi
EOF
    chmod +x /usr/local/bin/prhotspot_check.sh
    echo ${L_OK} 1>&3
}

_nginxSettings() {
    echo -n ${L_NGINXINSTALL} 1>&3
    cp /usr/local/prhotspot/install/prhotspot.sh /usr/local/etc/rc.d/prhotspot.sh
    chmod +x /usr/local/etc/rc.d/prhotspot.sh
    if [ ! -f /etc/rc.conf.local ] || [ $(grep -c prhotspot_enable /etc/rc.conf.local) -eq 0 ]; then
        echo 'prhotspot_enable="YES"' >> /etc/rc.conf.local
    fi
    sed -i .bak -e "s/{PR_PORT}/$PR_PORT/g" /usr/local/prhotspot/install/nginx-PRHotspot.conf
    echo ${L_OK} 1>&3
}

_radiusInstall() {

_deactiveRepos

    /usr/local/sbin/pfSsh.php playback listpkg | grep "pfSense-pkg-freeradius2"
    if [ $? == 0 ]
    then
    echo -n ${L_RADIUS2ALREADYINSTALLED} 1>&3
    /usr/local/sbin/pfSsh.php playback uninstallpkg "pfSense-pkg-freeradius2"
    fi
    /usr/local/sbin/pfSsh.php playback listpkg | grep "pfSense-pkg-freeradius3"
    if [ $? == 0 ]
    then
    echo -n ${L_RADIUSALREADYINSTALLED} 1>&3
    else
    echo -n ${L_RADIUSINSTALL} 1>&3
    /usr/local/sbin/pfSsh.php playback installpkg "pfSense-pkg-freeradius3"
    hash -r
    fi
    if [ ! -f /etc/rc.conf.local ] || [ $(grep -c radiusd_enable /etc/rc.conf.local) -eq 0 ]; then
        echo 'radiusd_enable="YES"' >> /etc/rc.conf.local
    fi
    echo ${L_OK} 1>&3
}


_cronInstall() {

_deactiveRepos()
    /usr/local/sbin/pfSsh.php playback listpkg | grep "cron"
    if [ $? == 0 ]
    then
    echo -n ${L_CRONALREADYINSTALLED} 1>&3
    else
    echo -n ${L_CRONINSTALL} 1>&3
    /usr/local/sbin/pfSsh.php playback installpkg "cron"
    hash -r
    fi
    echo ${L_OK} 1>&3
}

_prhotspotSettings() {
    echo -n ${L_QHOTSPOTSETTINGS} 1>&3
    cp /usr/local/prhotspot/install/prhotspotconfig.php /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_MYSQL_USER_NAME}/$PR_MYSQL_USER_NAME/g" /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_MYSQL_USER_PASS}/$PR_MYSQL_USER_PASS/g" /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_MYSQL_DBNAME}/$PR_MYSQL_DBNAME/g" /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_ZONE_NAME}/$PR_ZONE_NAME/g" /etc/phpshellsessions/prhotspotconfig
    /usr/local/sbin/pfSsh.php playback prhotspotconfig
    echo ${L_OK} 1>&3
}

_clean() {
    rm -rf ${START_PATH}/lang_*
    rm -rf /usr/local/prhotspot/install/client.cnf*
    rm -rf /usr/local/prhotspot/install/prhotspot.sql*
    rm -rf /usr/local/prhotspot/install/prhotspot.sh*
    rm -rf /usr/local/prhotspot/install/prhotspotconfig.php
}

YesOrNo() {
    while :
    do
        echo -n "$1 (yes/no?): " 1>&3
        read -p "$1 (yes/no?): " answer
        case "${answer}" in
            [yY]|[yY][eE][sS]) exit 0 ;;
                [nN]|[nN][oO]) exit 1 ;;
        esac
    done
}

main
