<script type="text/javascript">
		jQuery(document).ready(function() {
		 
		 	var $ = jQuery,
		 		$events_log = $("#events_log");
		 	$download = $("#download");
			$('#tree1').aciTree({
				ajax:{
					url: 'assets/js/aci-tree/php/aciTree.php?branch='
				},
				fullRow: true,
				
				// Columns
				columnData: [{
						props: 'type',
						width: 150
					}, {
						props: 'size',
						width: 100
					}]
		 
			})
			.on('acitree', function(event, api, item, eventName, options) {
		 
				// tell what tree it is
				var index = ($(this).attr('id') == 'tree1') ? 0 : 1;
		 
				switch (eventName) {
		 
				
		 
					case 'selected':
					 
						// Log select event
						var itemData = api.itemData(item);
					
						$events_log.val( "Seçilen Eleman: "  + api.getId(item) + " [Props size: "+itemData.size+"; type: "+itemData.type+" ]\n" + $events_log.val());
							
					
				if (itemData.type == "Folder")
				
				document.getElementById("download").innerHTML="Klasör";
			else
				document.getElementById("download").innerHTML='<a href="download.php?file=' + api.getId(item) + '">' + "Dosyayı İndir" + '</a>';
						break;
				}
		 
			});
		 
		});
		</script>

<div class="panel panel-primary" data-collapsed="0">
  <div class="panel-heading">
    <div class="panel-title"> 5651 Yasasına Uygun Kayıtlarınız </div>
    <div class="panel-options"> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> </div>
  </div>
  <div class="panel-body no-padding" style="padding: 5px 0;">
    <div id="tree1" class="aciTree aciTreeFullRow" style="min-height: 70px;"></div>
  </div>
  <div class="panel-body border-top">
    <textarea class="form-control" id="events_log" rows="5" placeholder="Logged Events"></textarea>
  </div>
   <div class="panel-body border-top" id="download">
 
  </div>
</div>

<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/js/aci-tree/css/aciTree.css">
<script src="assets/js/aci-tree/js/jquery.aciPlugin.min.js"></script> 

<!-- Imported scripts on this page --> 
<script src="assets/js/aci-tree/js/jquery.aciTree.min.js"></script> 
