<?php
	function microtime_diff($a, $b) {
	list($a_dec, $a_sec) = explode(" ", $a);
	list($b_dec, $b_sec) = explode(" ", $b);
	return $b_sec - $a_sec + $b_dec - $a_dec;
	}
	
	function test_speed($test_size) {
	flush();
	$start_time = microtime();
	$comment = "<!--O-->";
	$len = strlen($comment);
	for($i = 0; $i < $test_size; $i += $len) {
	echo $comment;
	}
	flush();
	$duration = microtime_diff($start_time, microtime());
	if($duration != 0) {
	return $test_size / $duration / 1024;
	}
	else {
	return log(0);
	}
	}
	
	$speed = test_speed(1024);
	if($speed > 50) { // a fast connection, send more byte for more accuracy
	$speed = test_speed(10240);
	if($speed > 500) { // a really fast connection, send even more byte for more accuracy
	$speed = test_speed(102400);
	}
	}

require_once("functions.inc");
require_once('notices.inc');
include_once("includes/functions.inc.php");

if ($_REQUEST['getupdatestatus']) {
	require_once("pkg-utils.inc");

	if (isset($config['system']['firmware']['disablecheck'])) {
		exit;
	}

	$system_version = get_system_pkg_version();

	if ($system_version === false) {
		print(gettext("<i>Unable to check for updates</i>"));
		exit;
	}

	if (!is_array($system_version) ||
	    !isset($system_version['version']) ||
	    !isset($system_version['installed_version'])) {
		print(gettext("<i>Error in version information</i>"));
		exit;
	}

	$version_compare = pkg_version_compare(
	    $system_version['installed_version'], $system_version['version']);

	switch ($version_compare) {
	case '<':
?>
		<div>
			<?=gettext("Version ")?>
			<span class="text-success"><?=$system_version['version']?></span> <?=gettext("is available.")?>
			<a class="fa fa-cloud-download fa-lg" href="/pkg_mgr_install.php?id=firmware"></a>
		</div>
<?php
		break;
	case '=':
		print(gettext("The system is on the latest version."));
		break;
	case '>':
		print(gettext("The system is on a later version than<br />the official release."));
		break;
	default:
		print(gettext( "<i>Error comparing installed version<br />with latest available</i>"));
		break;
	}

	exit;
}

$filesystems = get_mounted_filesystems();
?>

<table class="table table-striped table-hover">
	<tbody>
    <tr>
       <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
       <div id="chart_div" style="width: 600px; height: 120px;"></div>
    </tr>
		<tr>
			<th><?=gettext("Name");?></th>
			<td><?php echo htmlspecialchars($config['system']['hostname'] . "." . $config['system']['domain']); ?></td>
		</tr>
		<tr>
			<th><?=gettext("Version");?></th>
			<td>
5.0.0
			</td>
		</tr>
		<?php if (!$g['hideplatform']): ?>
		<tr>
			<th><?=gettext("Platform");?></th>
            
            
         
       
			<td>
				ProNex Network Solution
			</td>
		</tr>
		<?php endif; ?>
		
		<tr>
			<th><?=gettext("CPU Tipi");?></th>
			<td><?=htmlspecialchars(get_single_sysctl("hw.model"))?>
				<div id="cpufreq"><?= get_cpufreq(); ?></div>
		<?php
			$cpucount = get_cpu_count();
			if ($cpucount > 1): ?>
				<div id="cpucount">
					<?= htmlspecialchars($cpucount) ?> <?=gettext('CPUs')?>: <?= htmlspecialchars(get_cpu_count(true)); ?>
				</div>
		<?php endif; ?>
			</td>
		</tr>
		<?php if ($hwcrypto): ?>
		<tr>
			<th><?=gettext("Hardware crypto");?></th>
			<td><?=htmlspecialchars($hwcrypto);?></td>
		</tr>
		<?php endif; ?>
		<tr>
			<th><?=gettext("Açık Kalma Zamanı");?></th>
			<td id="uptime"><?= htmlspecialchars(get_uptime()); ?></td>
		</tr>
		
		<tr>
			<th><?=gettext("DNS server(s)");?></th>
			<td>
				<ul style="margin-bottom:0px">
				<?php
					$dns_servers = get_dns_servers();
					foreach($dns_servers as $dns) {
						echo "<li>{$dns}</li>";
					}
				?>
				</ul>
		</td>
		</tr>
	
		<tr>
			
		
		</tr>
	<?php if (get_temp() != ""): ?>
		
			
			
				<?php $temp_deg_c = get_temp(); ?>
		
		<?php endif; ?>
		
		
		<tr>
			
			
				<?php $memUsage = mem_usage(); ?>

				
			
		</tr>
		<?php if ($showswap == true): ?>
		<tr>
			<th><?=gettext("SWAP usage");?></th>
			<td>
				<?php $swapusage = swap_usage(); ?>
				<div class="progress">
					<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="<?=$swapusage?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$swapusage?>%">
					</div>
				</div>
				<span><?=$swapusage?>% of <?= sprintf("%.0f", `/usr/sbin/swapinfo -m | /usr/bin/grep -v Device | /usr/bin/awk '{ print $2;}'`) ?> MiB</span>
			</td>
		</tr>
		<?php endif; ?>

<?php


  $ch = curl_init();
  curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
  curl_setopt($ch, CURLOPT_URL, 'http://192.168.1.1:7445/topsites.cgi?year=2017&month=03&day=&mode=year&order=hits'); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $site = curl_exec($ch);
  curl_close($ch);
  
  // Veriyi parçalama işlemi
  preg_match_all('@<td>(.*?)</td>@si',$site,$veri_derece1);
  
        
  echo $veri_derece1[0][0];
 
  
 

?>

	</tbody>
</table>

<script type="text/javascript">
//<![CDATA[
<?php if (!isset($config['system']['firmware']['disablecheck'])): ?>
function systemStatusGetUpdateStatus() {
	$.ajax({
		type: 'get',
		url: '/widgets/widgets/system_information.widget.php',
		data: 'getupdatestatus=1',
		dataFilter: function(raw){
			// We reload the entire widget, strip this block of javascript from it
			return raw.replace(/<script>([\s\S]*)<\/script>/gi, '');
		},
		dataType: 'html',
		success: function(data){
			$('#widget-system_information #updatestatus').html(data);
		}
	});
}
<?php endif; ?>

function updateMeters() {
	url = '/getstats.php';

	$.ajax(url, {
		type: 'get',
		success: function(data) {
			response = data || "";
			if (response != "")
				stats(data);
		}
	});

	setTimer();

}

<?php if (!isset($config['system']['firmware']['disablecheck'])): ?>
events.push(function(){
	setTimeout('systemStatusGetUpdateStatus()', 4000);
});
<?php endif; ?>

/*   Most widgets update their backend data every 10 seconds.  11 seconds
 *   will ensure that we update the GUI right after the stats are updated.
 *   Seconds * 1000 = value
 */

var Seconds = 11;
var update_interval = (Math.abs(Math.ceil(Seconds))-1)*1000 + 990;

function setProgress(barName, percent) {
	$('#' + barName).css('width', percent + '%').attr('aria-valuenow', percent);
}

function setTimer() {
	timeout = window.setTimeout('updateMeters()', update_interval);
}

function stats(x) {
	var values = x.split("|");
	if ($.each(values,function(key,value){
		if (value == 'undefined' || value == null)
			return true;
		else
			return false;
	}))

	updateUptime(values[2]);
	updateDateTime(values[5]);
	updateCPU(values[0]);
	updateMemory(values[1]);
	updateState(values[3]);
	updateTemp(values[4]);
	updateInterfaceStats(values[6]);
	updateInterfaces(values[7]);
	updateCpuFreq(values[8]);
	updateLoadAverage(values[9]);
	updateMbuf(values[10]);
	updateMbufMeter(values[11]);
	updateStateMeter(values[12]);
}

function updateMemory(x) {
	if ($('#memusagemeter')) {
		$("#memusagemeter").html(x);
	}
	if ($('#memUsagePB')) {
		setProgress('memUsagePB', parseInt(x));
	}
}

function updateMbuf(x) {
	if ($('#mbuf')) {
		$("#mbuf").html('(' + x + ')');
	}
}

function updateMbufMeter(x) {
	if ($('#mbufusagemeter')) {
		$("#mbufusagemeter").html(x + '%');
	}
	if ($('#mbufPB')) {
		setProgress('mbufPB', parseInt(x));
	}
}

function updateCPU(x) {

	if ($('#cpumeter')) {
		$("#cpumeter").html(x + '%');
	}
	if ($('#cpuPB')) {
		setProgress('cpuPB', parseInt(x));
	}

	/* Load CPU Graph widget if enabled */
	if (widgetActive('cpu_graphs')) {
		GraphValue(graph[0], x);
	}
}

function updateTemp(x) {
	if ($("#tempmeter")) {
		$("#tempmeter").html(x + '&deg;' + 'C');
	}
	if ($('#tempPB')) {
		setProgress('tempPB', parseInt(x));
	}
}

function updateDateTime(x) {
	if ($('#datetime')) {
		$("#datetime").html(x);
	}
}

function updateUptime(x) {
	if ($('#uptime')) {
		$("#uptime").html(x);
	}
}

function updateState(x) {
	if ($('#pfstate')) {
		$("#pfstate").html('(' + x + ')');
	}
}

function updateStateMeter(x) {
	if ($('#pfstateusagemeter')) {
		$("#pfstateusagemeter").html(x + '%');
	}
	if ($('#statePB')) {
		setProgress('statePB', parseInt(x));
	}
}

function updateCpuFreq(x) {
	if ($('#cpufreq')) {
		$("#cpufreq").html(x);
	}
}

function updateLoadAverage(x) {
	if ($('#load_average')) {
		$("#load_average").html(x);
	}
}

function updateInterfaceStats(x) {
	if (widgetActive("interface_statistics")) {
		statistics_split = x.split(",");
		var counter = 1;
		for (var y=0; y<statistics_split.length-1; y++) {
			if ($('#stat' + counter)) {
				$('#stat' + counter).html(statistics_split[y]);
				counter++;
			}
		}
	}
}

function updateInterfaces(x) {
	if (widgetActive("interfaces")) {
		interfaces_split = x.split("~");
		interfaces_split.each(function(iface){
			details = iface.split("^");
			if (details[2] == '') {
				ipv4_details = '';
			} else {
				ipv4_details = details[2] + '<br />';
			}
			switch(details[1]) {
				case "up":
					$('#' + details[0] + '-up').css("display","inline");
					$('#' + details[0] + '-down').css("display","none");
					$('#' + details[0] + '-block').css("display","none");
					$('#' + details[0] + '-ip').html(ipv4_details);
					$('#' + details[0] + '-ipv6').html(details[3]);
					$('#' + details[0] + '-media').html(details[4]);
					break;
				case "down":
					$('#' + details[0] + '-down').css("display","inline");
					$('#' + details[0] + '-up').css("display","none");
					$('#' + details[0] + '-block').css("display","none");
					$('#' + details[0] + '-ip').html(ipv4_details);
					$('#' + details[0] + '-ipv6').html(details[3]);
					$('#' + details[0] + '-media').html(details[4]);
					break;
				case "block":
					$('#' + details[0] + '-block').css("display","inline");
					$('#' + details[0] + '-down').css("display","none");
					$('#' + details[0] + '-up').css("display","none");
					break;
			}
		});
	}
}
<?php
function son_sil($url){
    $us = strlen($url); //karkter sayısını alalım
    $sx = substr($url, 0, $us-1); //bu fonksiyon ile istediğimiz bölgeyi alıyoruz
    return $sx; //fonksiyon içinde olduğu için return ile dödürüyoruz
}
    ?>

function widgetActive(x) {
	var widget = $('#' + x + '-container');
	if ((widget != null) && (widget.css('display') != null) && (widget.css('display') != "none")) {
		return true;
	} else {
		return false;
	}
}

/* start updater */
events.push(function(){
	setTimer();
});
//]]>
</script>

<?php 
$kaynak = file_get_contents("/var/lightsquid/report/access.log");
$aranan = "'http://www.(.*?)/'si";
preg_match_all($aranan,$kaynak, $sonuc);
$toplam = array_count_values( $sonuc[0] );
$yaz = '';
 
?>
<?php 
$kaynak = file_get_contents("/var/lightsquid/report/access.log");
$aranan_ip = "'192.168.(.*?) 'si";
preg_match_all($aranan_ip,$kaynak, $sonuc_ip);
$toplam1 = array_count_values( $sonuc_ip[0] );
$yaz = '';
 
?>


       <script type="text/javascript">
             google.charts.load('current', {'packages':['gauge','corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
var mem =  "<?php echo $memUsage; ?>";
var cpu =  "<?php echo $cpucount; ?>";
var temp =  "<?php echo $temp_deg_c; ?>";
var speed =  "<?php echo $speed; ?>";
         

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['RAM', +mem],
          ['İşlemci', +cpu],
          ['Sıcaklık', +temp],
		     ['İnternet', +speed]
        ]);

        var options = {
          width: 700, height: 120,
          redFrom: 90, redTo: 100,
          yellowFrom:55, yellowTo: 90,
          minorTicks: 10
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);

       


      }
       google.charts.setOnLoadCallback(drawChart1);
        function drawChart1() {
            <?
            $yaz = "var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],";
$i=0;
arsort($toplam);
            foreach( $toplam as $key=>$values ){
            	if($i=="5") break; 
                $yaz .= "['$key', $values], ";
                $i++;
            }
            $son_sil = son_sil($yaz); // dosyayı kayıt edip sayffayı açarmısın
            echo son_sil($son_sil)."]);" ;
            ?>

            

        var baslik = {
          title: 'En Çok Ziyaret Edilen Siteler'
        };
        var chart = new  google.visualization.PieChart(document.getElementById('site_chart'));
        chart.draw(data, baslik);
      }
       google.charts.setOnLoadCallback(drawChart2);
        function drawChart2() {
            <?
            $yaz = "var data = google.visualization.arrayToDataTable([
['Task', 'Hours per Day'],";
$i=0;
arsort($toplam1);
            foreach( $toplam1 as $key=>$values ){
            	if($i=="5") break; 
                $yaz .= "['$key', $values], ";
                $i++;
            }
            $son_sil = son_sil($yaz); // dosyayı kayıt edip sayffayı açarmısın
            echo son_sil($son_sil)."]);" ;
            ?>

            

        var baslik = {
          title: 'En Çok Data Kullanan IP'
        };
        var chart = new  google.visualization.PieChart(document.getElementById('site_chart_ip'));
        chart.draw(data, baslik);
      }
	  </script>
